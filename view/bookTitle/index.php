<?php
include_once("../../vendor/autoload.php");

use App\BookTitle\BookTitle;
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo "<div id=\"message\">".Message::message()."</div>";

$obj = new BookTitle();
$arr = $obj->index();
$allData = $obj->index("OBJ");
$serial = 0;

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script>function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }</script>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>

<table class="table table-striped">
    <thead>
    <tr>
        <th>SERIAL</th>
        <th>ID</th>
        <th>BOOK TITLE</th>
        <th>AUTHOR NAME</th>
        <th class="col-sm-4">Actions</th>

    </tr>
    </thead>
    <?php foreach($allData as $oneData) {
    ?>
    <div class="col-sm-4">
    <tbody>
    <tr>
        <td><?php echo $serial++; ?></td>
        <td><?php echo $oneData->id; ?></td>
        <td><?php echo $oneData->book_title; ?></td>
        <td><?php echo $oneData->author_name; ?></td>
        <td>
            <a href="edit.php?id=<?php echo $oneData-> id ?>" class="btn btn-info" role="button">Edit</a>
            <a href="delete.php?id=<?php echo $oneData->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
            <a href="trash.php?id=<?php echo $oneData->id ?>"  class="btn btn-info" role="button">Trash</a>
        </td>    </tr>
    </tbody>
        </div>
<?php } ?>
    <script src="../../resource/assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../resource/assets/js/jquery.backstretch.min.js"></script>
    <script src="../../resource/assets/js/scripts.js"></script>
</body>
</html>

<script>
        $('#message').show().delay(10).fadeOut();

        $('#message').show().delay(10).fadeIn();
        $('#message').show().delay(1200).fadeOut();
        </script>
