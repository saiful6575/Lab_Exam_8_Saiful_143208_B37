<?php
include_once ('../../vendor/autoload.php');

use App\Hobbies\Hobbies;

$obj = new Hobbies();
$obj->setData($_GET);
$singleItem = $obj->view();
//var_dump($singleItem);die();
//Utility::d($singleItem);
$arrHobbies = explode(',',$singleItem->hobbies);

?>






<!DOCTYPE html>
<html lang="en">
<head>
    <title>Submitting Subscriber Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select Your Hobbies</h2>
    <form role="form" method="post" action="update.php">
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
        <input type="text" name="name" value="<?php echo $singleItem->username;?>">
        <div class="checkbox">
            <label><input type="checkbox" name="hobbies[]" value="Playing Cricket" <?php if(in_array("Playing Cricket",$arrHobbies)){echo "checked";} ?>>Playing Cricket</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="hobbies[]" value="Playing Football" <?php if(in_array("Playing Football",$arrHobbies)) {echo "checked";} ?> >Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobbies[]" value="Gaming" <?php if(in_array("Gaming",$arrHobbies)) {echo "checked";}?> >Gaming</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="hobbies[]" value="Watching Movies" <?php if(in_array("Watching Movies",$arrHobbies)) {echo "checked";}?>> Watching Movies</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="hobbies[]" value="Hanging Out" <?php if(in_array("Hanging Out",$arrHobbies)) : ?> checked <?php endif; ?> >Hanging Out</label>
        </div>
        <input type="submit" value="Submit">
    </form>

</div>

</body>
</html>
