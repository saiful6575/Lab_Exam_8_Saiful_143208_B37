<?php

namespace App\Birthday;

use App\Database as DB ;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
use PDOException;

class Birthday extends DB
{
    public $id;
    public $name;
    public $date;

    public function __construct()
    {

        parent::__construct();

    }
    public function index(){

        $STH = $this->conn->query('SELECT * FROM `birthday` WHERE `is_delete`=0');


        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;
    }

    public function delete(){
        $DBH = $this->conn;
        $STH = $DBH->prepare('DELETE from `birthday` WHERE `id`='.$this->id);
        $STH->execute();

        Message::message("<div id='msg'></div><h3 align='center'> <br> Data Has Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');
    }

    public function view(){
        $STH = $this->conn->query('SELECT * from birthday WHERE `id`='.$this->id);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $objAllData = $STH->fetchAll();
        return $objAllData;
    }

    public function update(){

        $data = array($this->name,$this->date);
        $STH = $this->conn->prepare("UPDATE `birthday` SET `username` = ? , `birthday` =? WHERE `id` =".$this->id);
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Birthday: $this->date ] <br> Data Has Been Updated Successfully!</h3></div>");


        Utility::redirect('index.php');
    }
    public function trash()
    {
        try {
            $query = "UPDATE `birthday` SET `is_delete` = ? WHERE `birthday`.`id` = ?";
            $STH = $this->conn->prepare($query);
            $STH->execute(array(1, $this->id));

            if ($STH) {
                Message::message("<div id='msg'></div><h3 align='center'> Data Has Been deleted Successfully!</h3></div>");
                Utility::redirect('index.php');
            }
        } catch (PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }


    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('date',$data)){
            $this->date = $data['date'];
        }
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->date);

        $STH = $DBH->prepare("INSERT INTO `birthday` (`id`, `username`, `birthday`) VALUES (NULL,?,?)");
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Birthday: $this->date ] <br> Data Has Been Inserted Successfully!</h3></div>");
        Utility::redirect('create.php');


    }


}// end of BookTitle class