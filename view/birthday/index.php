<?php
require_once("../../vendor/autoload.php");
use App\Message\Message;
use App\Birthday\Birthday;


if(!isset( $_SESSION)) session_start();
echo "<div id=\"message\">".Message::message()."</div>";



$obj = new Birthday();
$allData = $obj->index();
$serial = 0;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birthday-LIST</title>
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>

</head>
<body>
<table class="table table-striped">
    <thead>
    <tr>
        <th>SERIAL</th>
        <th>ID</th>
        <th>Name</th>
        <th>Birthday</th>
        <th class="col-sm-4">Actions</th>

    </tr>
    </thead>
    <?php foreach($allData as $oneData) {
    ?>
    <div class="col-sm-4">
        <tbody>
        <tr>
            <td><?php echo $serial++; ?></td>
            <td><?php echo $oneData->id; ?></td>
            <td><?php echo $oneData->username; ?></td>
            <td><?php echo $oneData->birthday; ?></td>
            <td>
                <!--<a href="view.php?id=<?php echo $oneData-> id ?>" class="btn btn-primary" role="button">View</a> -->
                <a href="edit.php?id=<?php echo $oneData-> id ?>"  class="btn btn-info" role="button">Edit</a>
                <a href="delete.php?id=<?php echo $oneData->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                <a href="trash.php?id=<?php echo $oneData->id ?>"  class="btn btn-info" role="button">Trash</a>
            </td>
        </tr>
        </tbody>
    </div>
<?php } ?>
</body>
<script src="../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../resource/assets/js/scripts.js"></script>
</html>
<script>
    setTimeout(fade_out, 4000);

        function fade_out() {
            $("#message").fadeOut().empty();
      }

</script>
