<?php

namespace App\Hobbies;

use App\Database as DB;

use PDO;
use PDOException;
use App\Message\Message;
use App\Utility\Utility;
class Hobbies extends DB
{

    public $id = "";

    public $name = "";

    public $hobbies = "";


    public function __construct()
    {

        parent::__construct();

    }
    public function index($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * FROM `hobbies` WHERE `is_delete`=0');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }
    public function view(){
        $query = "SELECT `hobbies` FROM hobbies WHERE `id`=".$this->id;
        $STH = $this->conn->query('SELECT * FROM hobbies WHERE `id`='.$this->id);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrayData = $STH->fetch();
        //$singleHobby = explode(",",$arrayData);
        return $arrayData;
    }


    public function update(){

        $DBH = $this->conn;
        $data = array($this->name,$this->hobbies,$this->id);
        $STH = $DBH->prepare('UPDATE `hobbies` SET  `username`= ?, `hobbies`=? WHERE `id`=?');
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Hobbies: $this->hobbies ] <br> Data Has Been Inserted Successfully!</h3></div>");
        Utility::redirect('index.php');
    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('hobbies',$data)){
            $hobby = implode(',',$data['hobbies']);
            $this->hobbies = $hobby;
        }
    }
    public function trash(){
        try{
            $query = "UPDATE `hobbies` SET `is_delete` = ? WHERE `hobbies`.`id` = ?";
            $STH = $this->conn->prepare($query);
            $STH->execute(array(1,$this->id));

            if($STH){
                Message::message("<div id='msg'></div><h3 align='center'> Data Has Been deleted Successfully!</h3></div>");
                Utility::redirect('index.php');
            }
        }
        catch(PDOException $e){
            echo 'Error:'.$e->getMessage();
        }}
    public function delete(){
        $DBH = $this->conn;
        $STH = $DBH->prepare('DELETE from `hobbies` WHERE `id`='.$this->id);
        $STH->execute();

        Message::message("<div id='msg'></div><h3 align='center'> <br> Data Has Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->hobbies);
        $STH = $DBH->prepare("INSERT INTO `hobbies`(`id`, `username`, `hobbies`) VALUES (NULL ,?,?)");
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Hobbies: $this->hobbies ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}