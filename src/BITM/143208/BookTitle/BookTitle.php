<?php

namespace App\BookTitle;

use App\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO;
use PDOException;


class BookTitle extends DB
{

    public $id = "";

    public $book_title = "";

    public $author_name = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('book_title',$data)){
            $this->book_title = $data['book_title'];
        }
        if(array_key_exists('author_name',$data)){
            $this->author_name = $data['author_name'];
        }
    }

    public function view(){
        $STH = $this->conn->query('SELECT * from book_title WHERE `id`='.$this->id);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $objAllData = $STH->fetchAll();
        return $objAllData;
    }
    public function trash(){
        try{
            $query = "UPDATE `book_title` SET `is_delete` = ? WHERE `book_title`.`id` = ?";
            $STH = $this->conn->prepare($query);
            $STH->execute(array(1,$this->id));

            if($STH){
                Message::message("<div id='msg'></div><h3 align='center'> Data Has Been deleted Successfully!</h3></div>");
                Utility::redirect('index.php');
            }
        }
        catch(PDOException $e){
            echo 'Error:'.$e->getMessage();
        }



    }

    public function update(){

        $data = array($this->book_title,$this->author_name);
        $STH = $this->conn->prepare("UPDATE `book_title` SET `book_title` = ? , `author_name` = ?  WHERE `id` =".$this->id);
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Been Updated Successfully!</h3></div>");
        Utility::redirect('index.php');
    }


    public function store(){
        $DBH = $this->conn;
        $data = array($this->book_title,$this->author_name);
        $STH = $DBH->prepare("INSERT INTO `book_title`(`id`, `book_title`, `author_name`) VALUES (NULL ,?,?)");
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

        
    }
    public function events($mode="ASSOC"){
        $mode = strtoupper($mode);
        $STH = $this->DBH->query('SELECT * FROM  birthday WHERE  DATE_ADD(birthday, INTERVAL YEAR(CURDATE())-YEAR(birthday) YEAR) 
                        BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY)
                        AND DATE_ADD(CURDATE(), INTERVAL 7 DAY) ORDER BY birthday ASC;');

        if($mode=="OBJ")
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData = $STH->fetchAll();

        return $arrAllData;

    } // view 7 days serial birth date



    public function delete(){
        $DBH = $this->conn;
        $STH = $DBH->prepare('DELETE from `book_title` WHERE `id`='.$this->id);
        $STH->execute();

        Message::message("<div id='msg'></div><h3 align='center'> <br> Data Has Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');
    }
    public function index($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * from book_title WHERE `is_delete`= 0');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;
    }public function trashed($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * from book_title WHERE `is_delete`= 1');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;
    }
    public function recover(){
        try{
            $query = "UPDATE `book_title` SET `is_delete` = ? WHERE `book_title`.`id` = ?";
            $STH = $this->conn->prepare($query);
            $STH->execute(array(0,$this->id));

            if($STH){
                Message::message("<div id='msg'></div><h3 align='center'> Data Has Been deleted Successfully!</h3></div>");
                Utility::redirect('trashed.php');
            }
        }
        catch(PDOException $e){
            echo 'Error:'.$e->getMessage();
        }
    }
    public function recoverSeleted($IDs = Array())
    {
        try {
            if ((is_array($IDs)) && (count($IDs > 0))) {
                $ids = implode(",", $IDs);
                $query = "UPDATE `atomic_project`.`book` SET `is_delete` = 0 WHERE `book`.`id` IN(" . $ids . ")";
                $STH = $this->conn->prepare($query);
                 $STH->execute();

                if ($STH) {
                    Message::message("<div id='msg'></div><h3 align='center'> Data Has Been Recovered Successfully!</h3></div>");
                    Utility::redirect('index.php');
                }

            }
        }catch(PDOException $e){
            echo 'ERROR:'.$e->getMessage();
        }




}
}