<?php
include_once("../../vendor/autoload.php");

use App\Email\Email;

$obj = new Email();
$allData = $obj->index("OBJ");
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>

</head>
<body>
<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Book Title</th>
        <th>Author Name</th>
        <th class="col-sm-4">Actions</th>

    </tr>
    </thead>
    <?php foreach($allData as $oneData) {
        ?>
        <div class="col-sm-4">
            <tbody>
            <tr>
                <td><?php echo $oneData->id; ?></td>
                <td><?php echo $oneData->username; ?></td>
                <td><?php echo $oneData->email; ?></td>
                <td><!--<a href="view.php?id=<?php echo $oneData-> id ?>" class="btn btn-primary" role="button">View</a> -->
                    <a href="edit.php?id=<?php echo $oneData-> id ?>"  class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $oneData->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                    <a href="trash.php?id=<?php echo $oneData->id ?>"  class="btn btn-info" role="button">Trash</a>
                </td>            </tr>
            </tbody>
        </div>
    <?php } ?>
</body>
</html>

