<?php
include_once("../../vendor/autoload.php");

use App\ProfilePicture\ProfilePicture;

$obj = new ProfilePicture();
$allData = $obj->index("OBJ");
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>

</head>
<body>
<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Picture</th>
        <th>Actions</th>

    </tr>
    </thead>
    <?php foreach($allData as $oneData) {
        $id = $oneData->id;
        $user = $oneData->username;
        $pic = $oneData->pro_pic;
        ?>
        <div class="col-sm-4">
            <tbody>
            <tr>
                <td><?php echo $id; ?></td>
                <td><?php echo $user; ?></td>
                <td><img src="../../resource/images/<?php echo $pic ?>" alt="<?php echo $oneData->pro_pic;?>" height="100px" width="100px" class="img img-responsive"> </td>
                <td><!--<a href="view.php?id=<?php echo $oneData->id ?>" class="btn btn-primary" role="button">View</a> -->
                    <a href="edit.php?id=<?php echo $oneData->id ?>" class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $oneData->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                    <a href="trash.php?id=<?php echo $oneData->id ?>"  class="btn btn-info" role="button" Onclick="return ConfirmDelete()">Trash</a>
                </td>
            </tr>
            </tbody>
        </div>
    <?php } ?>
    </table>
</body>
</html>

