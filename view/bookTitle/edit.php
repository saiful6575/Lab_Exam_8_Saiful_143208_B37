<?php
//var_dump($_GET);
include_once('../../vendor/autoload.php');
use App\BookTitle\BookTitle;
use App\Utility\Utility;

$obj= new BookTitle();
$obj->setData($_GET);
$oneData = $obj->view();
foreach($oneData as $singleItem) {
$id = $singleItem->id;
$title = $singleItem->book_title;
$author = $singleItem->author_name;
//Utility::d($singleItem);
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <!-- <title>CRUD-BOOK</title>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
     <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
     -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Book Title - Edit Form</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../resource/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../../resource/assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../resource/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../resource/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../resource/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../resource/assets/ico/apple-touch-icon-57-precomposed.png">

</head>
<body>

<div class="container">
    <h2>Edit Book Title</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit Book Title:</label>
            <input type="hidden" name="id" id="title" value="<?php echo $id?>">
            <input type="text" name="book_title" class="form-control" id="title" placeholder="Enter Book Title" value="<?php echo $title?>">
            <input type="text" name="author_name" class="form-control" id="author" placeholder="Enter Author Name" value="<?php echo $author?>">
            <?php } ?>
        </div>

        <button type="submit" value="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
