<?php

namespace App\Gender;

use App\Database as DB;

use PDO;
use PDOException;
use App\Message\Message;
use App\Utility\Utility;
class Gender extends DB
{
//echo $_server['Document_ROOT']
    public $id = "";

    public $name = "";

    public $gender = "";


    public function __construct()
    {

        parent::__construct();

    }
    public function index(){

        $STH = $this->conn->query('SELECT * FROM `gender` WHERE `is_delete`=0');

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }
    public function view(){
        $STH = $this->conn->query('SELECT * from gender WHERE `id`='.$this->id);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $objAllData = $STH->fetchAll();
        return $objAllData;
    }

    public function update(){

        $data = array($this->name,$this->gender);
        $STH = $this->conn->prepare("UPDATE `gender` SET `username` =  ? , `gender` = ? WHERE `id` =".$this->id);
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Gender: $this->gender ] <br> Data Has Been Updated Successfully!</h3></div>");


        Utility::redirect('index.php');
    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name= $data['name'];
        }
        if(array_key_exists('gender',$data)){
            $this->gender = $data['gender'];
        }

    }
    public function trash(){
        try{
            $query = "UPDATE `gender` SET `is_delete` = ? WHERE `gender`.`id` = ?";
            $STH = $this->conn->prepare($query);
            $STH->execute(array(1,$this->id));

            if($STH){
                Message::message("<div id='msg'></div><h3 align='center'> Data Has Been deleted Successfully!</h3></div>");
                Utility::redirect('index.php');
            }
        }
        catch(PDOException $e){
            echo 'Error:'.$e->getMessage();
        }}
    public function delete(){
        $DBH = $this->conn;
        $STH = $DBH->prepare('DELETE from `gender` WHERE `id`='.$this->id);
        $STH->execute();

        Message::message("<div id='msg'></div><h3 align='center'> <br> Data Has Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->gender);
        $STH = $DBH->prepare("INSERT INTO `gender`(`id`, `username`, `gender`) VALUES (NULL ,?,?)");
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Gender: $this->gender ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}