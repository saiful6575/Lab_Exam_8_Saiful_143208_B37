<?php
require_once("../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo "<div id=\"message\">".Message::message()."</div>";

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add hobbies</title>
    <link rel="stylesheet" href="../../resource/assets/css/style.css">

    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
</head>
<body>
<h2>Create hobbies</h2>
<form class="form-horizontal" method="post" action="store.php">
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">name:</label>
        <div class="col-sm-4">
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" size="10px">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">select hobbies:</label>
        <div class="col-sm-4">
            <div class="checkbox">
                <label><input type="checkbox" name="hobbies[]" value="Playing Cricket" >Playing Cricket</label>
            </div>
            <div class="checkbox disabled">
                <label><input type="checkbox" name="hobbies[]" value="Playing Football" >Playing Football</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobbies[]" value="Gaming" >Gaming</label>
            </div>
            <div class="checkbox disabled">
                <label><input type="checkbox" name="hobbies[]" value="Watching Movies" > Watching Movies</label>
            </div>
            <div class="checkbox disabled">
                <label><input type="checkbox" name="hobbies[]" value="Hanging Out" >Hanging Out</label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="submit" class="btn btn-info">Create</button>
        </div>
    </div>
</form>
<script src="../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../resource/assets/js/scripts.js"></script>
</body>
</html>
<script>
    //    $('#message').show().delay(10).fadeOut();
    //    $('#message').show().delay(10).fadeIn();
    //    $('#message').show().delay(10).fadeOut();
    //    $('#message').show().delay(10).fadeIn();
    //    $('#message').show().delay(1200).fadeOut();
    setTimeout(fade_out, 4000);

    function fade_out() {
        $("#message").fadeOut("").empty();
    }
</script>