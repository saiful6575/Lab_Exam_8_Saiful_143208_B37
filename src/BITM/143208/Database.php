<?php
namespace App;

use PDO;
use PDOException;


class Database{
    public $conn;

    public $host="localhost";
    public $dbname="atomic_project_b37";
    public $username="root";
    public $password="";


    public function __construct()
    {
        try {

            # MySQL with PDO_MYSQL
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=".$this->dbname, $this->username, $this->password);

        }
        catch(PDOException $e) {
            echo "unsuccess";
            echo $e->getMessage();
        }
    }
}
