<?php

namespace App\SummeryOfOrganization;

use App\Database as DB;

use PDO;
use App\Message\Message;
use App\Utility\Utility;
class SummeryOfOrganization extends DB
{

    public $id = "";

    public $name = "";

    public $summery = "";


    public function __construct()
    {

        parent::__construct();

    }
    public function index($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * FROM `summary_of_organization` WHERE `is_delete`=0');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }
    public function view(){
        $STH = $this->conn->query('SELECT * from summary_of_organization WHERE `id`='.$this->id);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $objAllData = $STH->fetchAll();
        return $objAllData;
    }

    public function update(){

        $data = array($this->name,$this->summery,$this->id);
        $STH = $this->conn->prepare("UPDATE `summary_of_organization` SET `name` = ? , `summery` = ? WHERE `id` =?");
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name of organization: $this->name ] , [ summary_of_organization: $this->author_name ] <br> Data Has Been Updated Successfully!</h3></div>");


        Utility::redirect('index.php');
    }


    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('summery',$data)){
            $this->summery = $data['summery'];
        }
    }
    public function delete(){
        $DBH = $this->conn;
        $STH = $DBH->prepare('DELETE from `summary_of_organization` WHERE `id`='.$this->id);
        $STH->execute();

        Message::message("<div id='msg'></div><h3 align='center'> <br> Data Has Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');
    }
    public function trash(){
        try{
            $query = "UPDATE `summary_of_organization` SET `is_delete` = ? WHERE `summary_of_organization`.`id` = ?";
            $STH = $this->conn->prepare($query);
            $STH->execute(array(1,$this->id));

            if($STH){
                Message::message("<div id='msg'></div><h3 align='center'> Data Has Been deleted Successfully!</h3></div>");
                Utility::redirect('index.php');
            }
        }
        catch(PDOException $e){
            echo 'Error:'.$e->getMessage();
        }}

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->summery);
        $STH = $DBH->prepare("INSERT INTO `summary_of_organization`(`id`, `name`, `summery`) VALUES (NULL ,?,?)");
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[ Name of the Organization: $this->name ] , [ Summery of the organization: $this->summery ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}